##################################
#
#   Utilities used for signalProcessing
#
#
#
#
# Author: Damian Kula
# March 2016
##################################
import ZODB
import ZODB.FileStorage
from bs4 import BeautifulSoup
import scipy.io
import numpy
import BTrees.OOBTree
import station
from obspy.io.xseed import Parser
import os




def db_connect():
    storage = ZODB.FileStorage.FileStorage('database\stationdatabase.fs')
    db = ZODB.DB(storage)
    connection = db.open()
    root = connection.root
    return connection, root, db

def parse_event_names(file):
    soup = BeautifulSoup(open(file),'lxml-xml')
    parsed_meta = {}
    for ev in soup.find_all('event'):
        parsed_meta[str(ev.file.contents[0])] = ev['id']
    return parsed_meta


def parse_catalog(file):
    mat = scipy.io.loadmat(file)

    cata_len = len(mat['Catalog'][0][0][2])

    catalog = {
       'eq_id':
               [mat['Catalog'][0][0][2][i][0][0] for i in range(cata_len)],
        'time':
               [mat['Catalog'][0][1][2][i][0] for i in range(cata_len)],
        'lat':
               [mat['Catalog'][0][2][2][i][0] for i in range(cata_len)],
        'lon':
               [mat['Catalog'][0][3][2][i][0] for i in range(cata_len)],
        'depth':
               [mat['Catalog'][0][4][2][i][0] for i in range(cata_len)],
        'elevation':
               [mat['Catalog'][0][5][2][i][0] for i in range(cata_len)],
        'mw':
               [mat['Catalog'][0][13][2][i][0] for i in range(cata_len)]}

    return catalog


def konno(freq_vector, band_for_konno, data_vector):
    mean_edge_freq = []
    smoothed_spectrum = []

    for i in range(len(freq_vector)-band_for_konno):
        freq_temp = freq_vector[i + 1:i + band_for_konno + 1]
        mean_edge_freq.append(numpy.sqrt( freq_temp[0]*freq_temp[-1]))
        wagi = []
        for k in range(band_for_konno):
            if freq_temp[k] == mean_edge_freq[i]:
                wagi.append(1)
            else:
                wagi.append((numpy.sin(band_for_konno * numpy.log10(freq_temp[k] / mean_edge_freq[i])) / (band_for_konno * numpy.log10(freq_temp[k] / mean_edge_freq[i]))) ** 4)

        jj = sum(wagi)
        smoothed_spectrum.append(sum((data_vector[i + 1:i + band_for_konno + 1] * wagi) / sum(wagi)))


    return smoothed_spectrum, mean_edge_freq


def parse_dataless_files(directory):
    import os
    from obspy.io.xseed import Parser
    import transaction

    connection, root, db = db_connect()

    file_list = os.listdir(path=directory)

    connection.open()
    for file in file_list:
        parsed = Parser(file)
        station_id = parsed.blockettes[11][-1].station_identifier_code
        if  station_id in root.stations.keys():
            root.stations[station_id].add_dataless(parsed)
    transaction.commit()
    connection.close()
    return


def calc_and_print(station, noise_no,root):
    #
    win_lens = [10,20,40,60,80,120,160,200,240,360,480]


    for le in win_lens:
        print('Calculating hvsr for window ',le, ' seconds.')
        root.stations[station].noise[noise_no].calc_hvsr(len_window = le)

        root.stations[station].noise[noise_no].calculated_hvsr = True


        root.stations[station].hvsr_noise[-1].plot_av_hvsr(y_lim=(0,6), show_fig = False)

    for i in range(10, root.stations[station].hvsr_noise[-1].no_avg_windows, 10):
        print('Printing av hvsr averaged for ', i, ' windows.')
        root.stations[station].hvsr_noise[-1].calc_av_hvsr(no_windows = i)
        root.stations[station].hvsr_noise[-1].plot_av_hvsr(y_lim=(0,6), show_fig = False)



# Function checks if 3 traces in row are from same station and time
def check_if_same_event(seed_data, i):
    consistency = False
    if (seed_data[i + 0].stats.station == seed_data[i + 1].stats.station and
                seed_data[i + 1].stats.station == seed_data[i + 2].stats.station):
        consistency = True
    return consistency


def check_events():
    for stat in list(root.stations.keys()):
        print('there are ', len(root.stations[stat].events),
              ' in station ', stat)
    return


def check_and_create_station(station_id, root):
    dataless_list = os.listdir(path='D:\\Python\\signalProcessing\\dataless')

    if not hasattr(root, 'stations'):
        root.stations = BTrees.OOBTree.BTree()
    if station_id not in root.stations.keys():
        root.stations.insert(station_id, station.Station(station_id))
        root.stations[station_id].dataless = Parser(
        ''.join(['D:\\Python\\signalProcessing\\dataless\\', [s for s in dataless_list if station_id in s][0]]))
    return

def create_station_and_add_dataless(station_id):
    dataless_list = os.listdir(path='dataless')

    ret = station.Station(station_id)
    ret.dataless = Parser(
        ''.join(['dataless\\', [s for s in dataless_list if station_id in s][0]]))
    return ret


def plot_all_hvsr_for_each():
    for stat in root.stations:
        root.stations[stat].plot_all_hvsr()
    return

def save_all_hv_data():
    for stat in root.stations:
        scipy.io.savemat(''.join(['hv/', stat, '_event.mat']),
                         {'fHV': root.stations[stat].av_freq_vec,
                          'HVSR': root.stations[stat].av_hvsr})
        for hv_noise in root.stations[stat].hvsr_noise:
            print(stat)
            hv_noise.export_to_mat()