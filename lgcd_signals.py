import obspy
import os
import transaction
import utilities
import ZODB.FileStorage

os.chdir('D:\\szum_lgcd')
if not os.path.exists('database/'):
    os.makedirs('database/')
if not os.path.exists('plots'):
    os.makedirs('plots')
if not os.path.exists('noise/used'):
    os.makedirs('noise/used')

if 'storage' not in locals():
    storage = ZODB.FileStorage.FileStorage('database\lgcd_signals.fs')
    db = ZODB.DB(storage, pool_size=16, large_record_size = 186405978)
    connection = db.open()
    root = connection.root
    if not os.path.exists('database/files/'):
        os.makedirs('database/files/')
#    connection.close()


# Import list of dataless files
dataless_list = os.listdir(path='D:\\Python\\signalProcessing\\dataless')

# Parse xml with list of events and qarthquakes
earthquakes = utilities.parse_event_names('metadata_signals_LGCD.xml')

# Parse matfile with catalog
catalog = utilities.parse_catalog('LGCD_catalog.mat')

# Check the filelist
file_list = [f for f in os.listdir('dane') if os.path.isfile(''.join(['dane/', f]))]

connection.open()
# Iterate over each file in given list
for seed_name in file_list:
    # Diagnostic printout
    print('Processing ', seed_name, ' file.')
    # Read seed
    seed_data = obspy.read(''.join(['dane/', seed_name]))
    # Parse seed for characteristics

    # Iterate over traces and add events to stations
    for i in range(0, len(seed_data), 3):

        # Check if traces are from the same station and save them as event
        if utilities.check_if_same_event(seed_data, i):
            # Save informations about station
            station_name = seed_data[i].stats.station

            # Check if station exists and create it if needed
            utilities.check_and_create_station(station_name, root)

            # Add event to events list
            root.stations[station_name].add_event(seed_name, earthquakes[seed_name],
                      {seed_data[i].stats.channel[-1]: seed_data[i],
                       seed_data[i + 1].stats.channel[-1]: seed_data[i+1],
                       seed_data[i + 2].stats.channel[-1]: seed_data[i+2]})
        else:
            print('There is a problem with event in file ',
                             seed_name, ' Event starts with trace no ', i)
            break

    # root.stations[station_name].events._p_changed = True
    os.rename(''.join(['dane/', seed_name]), ''.join(['dane/used/', seed_name]))
    transaction.commit()
connection.close()

# Check how many events in each station
connection.open()
utilities.check_events()
connection.close()


# Calculate HVSRs for each event and average hvsr
connection.open()
j = 0
for name in ['MOSK','PEKW','PCHB']:
    print('Processing station ', name)
    for i,event in enumerate(root.stations[name].events):
        j += 1
        if not hasattr(event, 'hvsr'):
            print('Calculating hvsr for event no ', i)
            event.calc_hvsr()
    if not hasattr(root.stations[name], 'av_hvsr'):
        print('Calculating average HVSR for station', name)
        root.stations[name].calc_av_hvsr()

    root.stations[name].plot_av_hvsr()
connection.close()
