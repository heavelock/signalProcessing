#!/usr/bin/python

#By Janusz Mirek, jmirek@igf.edu.pl, 2016

from obspy import UTCDateTime
from obspy.clients.arclink.client import Client
from obspy.core.stream import Stream
import re

#connect to arclink server
SHEER_user='mlasak@igf.edu.pl' #your email
SHEER_DCID='SHEER'             
SHEER_pass='zcjtdcRI'          #your pass
SHEER_host='sheerwer.igf.edu.pl'
SHEER_port=18001
client = Client(user=SHEER_user,  dcid_keys={SHEER_DCID:SHEER_pass}, host=SHEER_host,port=SHEER_port)

#get data
sig_length = 600
network = "PL"
station = "PLA8"
location = ""
channel = "*"
time_start = UTCDateTime("2016-04-01 01:00:00")
time_end = time_start + sig_length   #10min = 360sec
st = client.get_waveforms(network, station, location, channel, time_start, time_end, route=0)
time_start_str = re.sub('[^A-Za-z0-9]+', '',str(time_start))
time_end_str = re.sub('[^A-Za-z0-9]+', '',str(time_end))

#save data to file
st.write(''.join([network,station,time_start_str,"_",time_end_str,'.mseed']), format='MSEED')


