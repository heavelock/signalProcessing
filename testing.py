# -*- coding: utf-8 -*-

import numpy
import matplotlib.pyplot as plt
import station
from event import Event
from noise import Noise
import obspy
import os
from obspy.io.xseed import Parser
import transaction
import utilities
import BTrees.OOBTree
import ZODB
import ZODB.FileStorage


if 'storage' not in locals():
    storage = ZODB.FileStorage.FileStorage('database\stationdatabase.fs')
    db = ZODB.DB(storage, pool_size=16, large_record_size = 67108864)
    connection = db.open()
    root = connection.root

#    connection.close()


# Function checks if 3 traces in row are from same station and time
def check_if_same_event(seed_data, i):
    consistency = False
    if (seed_data[i + 0].stats.station == seed_data[i + 1].stats.station and
                seed_data[i + 1].stats.station == seed_data[i + 2].stats.station):
        consistency = True
    return consistency



def check_events():
    for stat in list(root.stations.keys()):
        print('there are ', len(root.stations[stat].events),
              ' in station ', stat)
    return



def check_and_create_station(station_id, root):
    if not hasattr(root, 'stations'):
        root.stations = BTrees.OOBTree.BTree()
    if station_id not in root.stations:
        root.stations[station_id] = station.Station(station_id)
        root.stations[station_id].dataless = Parser(
        ''.join(['dataless/', [s for s in dataless_list if station_id in s][0]]))
    return


def print_hvsrs():
    for stat in root.stations:
        root.stations[stat].plot_all_hvsr()
    return

self = root.stations['MOSK']
hv = plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')
for i in range(len(self.noise)):
    if i == 0:
        plt.plot(self.noise[i].av_freq_vec, self.noise[i].av_hvsr, color='#abc1d4', label = 'Noise HVSR')
    else:
        plt.plot(self.noise[i].av_freq_vec, self.noise[i].av_hvsr, color='#abc1d4')

# Plot average HVSR line
ev_hv = plt.plot(self.av_freq_vec, self.av_hvsr, 'r', label = 'Averaged event HVSR')
a,b = utilities.konno(numpy.array(self.av_freq_vec),int(numpy.fix((1/100 * 25 * len(self.av_freq_vec)))) , numpy.array(self.av_hvsr))
plt.plot(b,a, 'g--', )
plt.plot(self.precalc_hv[:,0],self.precalc_hv[:,1])
# Add description to plot
plt.xlabel('Frequency (Hz)')
plt.ylabel('HVSR')
plt.title(" ".join(['H/V ratio for',self.id,'station, calculated using noise and events']))
plt.xscale('log')
plt.xlim((0.5,30))
plt.ylim((0,4))
plt.legend()
plt.show()
hv.savefig("".join(['plots/',self.id,'_all_HVSR.png']), dpi=150)