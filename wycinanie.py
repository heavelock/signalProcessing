import os
import obspy

seed_list = os.listdir(path='dane')
station_list = os.listdir(path='csv')


for sta in station_list:
    f = open(''.join(['csv/',sta]))
    station_name = sta[0:-4]

    for i, line in enumerate(f.readlines()):
        if i == 0:
            continue

        filename = line.split(';')[0]

        seed_data = obspy.read(''.join(['dane/',filename]))

        # Iterate over traces and add events to stations
        for i in range(0, len(seed_data), 3):
            if seed_data[i].stats.station == station_name:
                seed_data[i:i+3].write(''.join(['wyjscie/',station_name,'_',filename]),'mseed')
            else:
                continue