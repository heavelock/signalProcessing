# -*- encoding: utf-8 -*-
# begin

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, BigInteger,String, ForeignKey, Unicode, Binary, LargeBinary, Time, DateTime, Date, Text, Boolean, Float
from sqlalchemy.orm import relationship, backref, deferred
from sqlalchemy.orm import sessionmaker

Base = declarative_base()



class Stations (Base):
    __tablename__ = "Stations"
    station_id = Column('station_id', Unicode, primary_key = True)
    dataless = deferred(Column('dataless', Text))
    station_name = deferred(Column('station_name', Text))
    station_directory = deferred(Column('station_directory', Text))
    directory = deferred(Column('directory', Text))

class Events (Base):
    __tablename__ = "events"
    event_id = deferred(Column('event_id', Text, ForeignKey('catalog.events_event_id'), primary_key = True))
    starttime = Column('starttime', DateTime)
    stations_no = Column('stations_no', Integer)
    original_file_location = deferred(Column('original_file_location', Text))

    catalog = relationship('Catalog', foreign_keys=event_id)

class ProcessedEvents (Base):
    __tablename__ = "processed_events"
    stations_station_id = Column('Stations_station_id', Unicode, ForeignKey('Stations.station_id'), primary_key = True)
    events_event_id = deferred(Column('events_event_id', Text, ForeignKey('events.event_id'), primary_key = True))
    starttime = Column('starttime', DateTime)
    directory = deferred(Column('directory', Text))
    signal_type = Column('signal_type', Unicode)
    catalog_earthquake_id = deferred(Column('catalog_earthquake_id', Text, ForeignKey('catalog.earthquake_id')))
    calculated_hvsr = Column('calculated_hvsr', Boolean)
    a_e = Column('a_e', Float)
    a_n = Column('a_n', Float)
    a_z = Column('a_z', Float)
    a_en = Column('a_en', Float)
    a_enz = Column('a_enz', Float)
    a10_e = Column('a10_e', Float)
    a10_n = Column('a10_n', Float)
    a10_z = Column('a10_z', Float)
    a10_en = Column('a10_en', Float)
    a10_enz = Column('a10_enz', Float)
    v_max_e = Column('v_max_e', Float)
    v_max_n = Column('v_max_n', Float)
    v_max_z = Column('v_max_z', Float)
    v_max_en = Column('v_max_en', Float)
    v_max_enz = Column('v_max_enz', Float)
    original_file = deferred(Column('original_file', Text))
    trimmed_signals_file = deferred(Column('trimmed_signals_file', Text))
    removed_response_file = deferred(Column('removed_response_file', Text))
    hvsr_file = deferred(Column('hvsr_file', Text))

    stations = relationship('Stations', foreign_keys=stations_station_id)
    events = relationship('Events', foreign_keys=events_event_id)
    catalog = relationship('Catalog', foreign_keys=catalog_earthquake_id)

class Noise (Base):
    __tablename__ = "noise"
    stations_station_id = Column('Stations_station_id', Unicode, ForeignKey('Stations.station_id'), primary_key = True)
    starttime = Column('starttime', Integer, primary_key = True)

    stations = relationship('Stations', foreign_keys=stations_station_id)

class Catalog (Base):
    __tablename__ = "catalog"
    earthquake_id = deferred(Column('earthquake_id', Text, primary_key = True))
    events_event_id = deferred(Column('events_event_id', Text))
    time = Column('time', DateTime)
    latitude = deferred(Column('latitude', Text))
    longitude = deferred(Column('longitude', Text))
    depth = Column('depth', Float)
    elevation = Column('elevation', Float)
    mw = Column('mw', Float)
    log_e = Column('log_e', Float)

# end
