import station
import obspy
import os
from obspy.io.xseed import Parser
import utilities
import scipy
import event
import matplotlib.pyplot as plt
import scipy.io


# Import list of dataless files
dataless_list = os.listdir(path='dataless')

# Parse xml with list of events and qarthquakes
earthquakes = utilities.parse_event_names('metadata_signals_LGCD.xml')

# Parse matfile with catalog
catalog = utilities.parse_catalog('LGCD_catalog.mat')

# Check the filelist
file_list = [f for f in os.listdir('dane') if os.path.isfile(''.join(['dane/', f]))]

# Allocate stations
stations = {}
results = {}


# Iterate over each file in given list
for seed_name in file_list:
    # Diagnostic printout
    print('Processing ', seed_name, ' file.')
    # Read seed
    seed_data = obspy.read(''.join(['dane/', seed_name]))
    # Parse seed for characteristics

    # Iterate over traces and add events to stations
    for i in range(0, len(seed_data), 3):

        # Check if traces are from the same station and save them as event
        if not utilities.check_if_same_event(seed_data, i):
            print('Something bad occured. Breaking')
            break
        # Save informations about station
        station_name = seed_data[i].stats.station

        # Check if station exists and create it if needed
        if station_name not in stations.keys():
            stations[station_name] = utilities.create_station_and_add_dataless(station_name)
            results[station_name] = []

        # create an event object
        try:
            event_object = event.Event(seed_name,
                                       earthquakes[seed_name],
                                       {seed_data[i].stats.channel[-1]: seed_data[i],
                                        seed_data[i + 1].stats.channel[-1]: seed_data[i+1],
                                        seed_data[i + 2].stats.channel[-1]: seed_data[i+2]},
                                       stations[station_name])
        # Catch exception
        except KeyError:
            print('There is something wrong with a file ', seed_name)
            break


        # Determine signal type and skip iteration if velocity signal occurs
        if event_object.determine_signal_type() == 'VEL':
            print('Velocity signal, breaking')
            continue
        # Print diagnostic message if acc signal occurs
        print('Found acceleration signal, processing! Signal from station', station_name)

        # Add information downloaded from catalog file
        event_object.add_catalog_data(catalog)

        # Set lowpass filter parameters
        event_object.lowpass_filter_freq = 10
        event_object.lowpass_filter_order = 5

        # Backup original traces
        event_object.e = event_object.orig_e.copy()
        event_object.n = event_object.orig_n.copy()
        event_object.z = event_object.orig_n.copy()

        # Remove response from traces
        try:
            event_object.e_resp_removed = event_object.remove_response('e')
            event_object.n_resp_removed = event_object.remove_response('n')
            event_object.z_resp_removed = event_object.remove_response('z')
        except ValueError:
            print('Brakuje datalessa dla stacji ', station_name)

        # Calc A max
        event_object.calc_a_max(event_object.e_resp_removed,
                                event_object.n_resp_removed,
                                event_object.z_resp_removed)

        # Narysuj ploty
        event_object.draw_plots_orig_and_filtered(event_object.e_resp_removed,
                                                  event_object.n_resp_removed,
                                                  event_object.z_resp_removed,
                                                  'highpass')

        event_object.visual_check = input('Sygnał dobry=0, zniekształcony=1, slaby=2, krótki=3, dobry po filtracji=5\n')

        plt.close()

        filter_freq = 1

        while True:
            event_object.e_vel, \
            event_object.n_vel, \
            event_object.z_vel = event_object.draw_vel_plots(event_object.e_resp_removed,
                                                              event_object.n_resp_removed,
                                                              event_object.z_resp_removed,
                                                              filter_freq=filter_freq)
            filter_freq = input('Do you want to change filtering freq before intergating signal? \n')
            try:
                filter_freq = float(filter_freq)
            except ValueError:
                break

        event_object.calc_v_max(event_object.e_vel,
                                event_object.n_vel,
                                event_object.z_vel)

        plt.close('all')

        results[station_name].append([event_object.signal_id,event_object.earthquake_id,
                                      event_object.visual_check,
                                      event_object.cat_time,
                                      event_object.a_e, event_object.a_n, event_object.a_z,
                                      event_object.a_en, event_object.a_enz,
                                      event_object.a10_e, event_object.a10_n, event_object.a10_z,
                                      event_object.a10_en, event_object.a10_enz,
                                      event_object.v_max_e, event_object.v_max_n, event_object.v_max_z,
                                      event_object.v_max_en, event_object.v_max_enz,
                                      ])

        scipy.io.savemat('wyniki.mat', results)

print('That\'s all, saving results')

