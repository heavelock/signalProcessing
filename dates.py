import numpy
import matplotlib.pyplot as plt
from matplotlib.finance import quotes_historical_yahoo_ochl
from matplotlib.dates import YearLocator, MonthLocator,DayLocator, DateFormatter
import datetime


def format_those(ax, fig):
    # format the ticks
    months = MonthLocator()  # every month
    monthsFmt = DateFormatter('%b')
    ax.xaxis.set_major_locator(months)
    ax.xaxis.set_major_formatter(monthsFmt)
    ax.autoscale_view()
    ax.fmt_xdata = DateFormatter('%m')
    ax.grid(True)
    ax.ylim = (-22, 10)

    fig.autofmt_xdate()

    plt.show()
    return


da2012 = numpy.load('mapy//temperatury//temp2012.npy')
da2013 = numpy.load('mapy//temperatury//temp2013.npy')
da2014 = numpy.load('mapy//temperatury//temp2014.npy')

syg2012noc = numpy.load('mapy//datysygnalow//2012nocdatysygnalow.npy')
syg2012dzien = numpy.load('mapy//datysygnalow//2012dziendatysygnalow.npy')
syg2013noc = numpy.load('mapy//datysygnalow//2013nocdatysygnalow.npy')
syg2013dzien = numpy.load('mapy//datysygnalow//2013dziendatysygnalow.npy')
syg2014noc = numpy.load('mapy//datysygnalow//2014nocdatysygnalow.npy')
syg2014dzien = numpy.load('mapy//datysygnalow//2014dziendatysygnalow.npy')





fig, (ax1) = plt.subplots(1)
ax1.plot_date(da2013[:,0], da2013[:,1], '-')
ax1.plot_date(syg2013noc,[-20]*len(syg2013noc),'*')
ax1.plot_date(syg2013dzien,[-20.5]*len(syg2013dzien),'*')
# ax1.set_title('Rok 2012')
format_those(ax1,fig)
# # fig, ax = plt.subplots()
# ax2.plot_date(da2013[:,0], da2013[:,1], '-')
# ax2.plot_date(syg2013noc,[-20]*len(syg2013noc),'x')
# ax2.plot_date(syg2013dzien,[-20.5]*len(syg2013dzien),'x')
# ax2.set_title('Rok 2013')
# format_those(ax2,fig)
# # fig, ax = plt.subplots()
# ax3.plot_date(da2014[:,0], da2014[:,1], '-')
# ax3.plot_date(syg2014noc,[-20]*len(syg2014noc),'|')
# ax3.plot_date(syg2014dzien,[-20.5]*len(syg2014dzien),'|')
# ax3.set_title('Rok 2014')
# format_those(ax3,fig)
#
