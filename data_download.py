import re
import obspy
from obspy import UTCDateTime
# from obspy.clients.fdsn import Client
from obspy.clients.arclink.client import Client
from multiprocessing import Pool
import os.path
import numpy

# client = Client("IRIS")

SHEER_user='user' #your email
SHEER_DCID=''
SHEER_pass=''          #your pass
SHEER_host='tytan.igf.edu.pl'
SHEER_port=18001
client = Client(user=SHEER_user, host=SHEER_host,port=SHEER_port)

sig_len = 60 * 60 * 6
network = 'PL'
station = 'HSPB'
location = ''
channels = '*'

first_start_time = UTCDateTime('2012-01-01T09:00:00.000')


def download_data(iteration_no):
    while True:
        start_time = first_start_time + iteration_no * 60 * 60 * 24
        end_time = start_time + sig_len

        start_time_str = re.sub('[^A-Za-z0-9]+', '', str(start_time))
        end_time_str = re.sub('[^A-Za-z0-9]+', '', str(end_time))

        filename = ''.join(['pobrane_dane/', network, station, start_time_str, "_", end_time_str, '.mseed'])
        if os.path.isfile(filename):
            print('File with data from ', start_time_str, ' exists, skipping.')
            ret = [str(1), str(start_time.date)]
            break

        try:
            print('Trying to download data from station ', station, ' starting on ', start_time_str)
            st = client.get_waveforms(network, station, location, channels, start_time, end_time)
        except ConnectionResetError:  # Obsuga wyjatku zwiazanego z polaczeniem
            print('ConnectionResetError occured! Retrying!')
            continue
        # except obspy.clients.fdsn.header.FDSNException:
        except obspy.clients.arclink.client.ArcLinkException:
            print('Data missing. Skipping day ', start_time_str)
            ret = [str(0), str(start_time.date)]
            break

        print('Data downloaded, saving to file!')

        # save data to file
        st.write(filename, format='MSEED')
        print('Data from ', start_time_str, ' saved!')
        ret = [str(1), str(start_time.date)]
        break
    return ret


if __name__ == '__main__':
    with Pool(8) as p:
        wynik = p.map(download_data, range(1460))
        f = open('wyjscie.txt', 'w')
        for line in wynik:
            f.write(' '.join(line))
            f.write('\n')
        f.close()

