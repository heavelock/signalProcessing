import station
import obspy
import os
from obspy.io.xseed import Parser
import transaction
import utilities
import BTrees.OOBTree
import ZODB
import ZODB.FileStorage
import scipy

os.chdir('D:\\szum_lgcd')
if not os.path.exists('database/'):
    os.makedirs('database/')
if not os.path.exists('plots'):
    os.makedirs('plots')
if not os.path.exists('noise/used'):
    os.makedirs('noise/used')

if 'storage' not in locals():
    storage = ZODB.FileStorage.FileStorage('database\lgcd_noise.fs')
    db = ZODB.DB(storage, pool_size=16, large_record_size = 186405978)
    connection = db.open()
    root = connection.root
    if not os.path.exists('database/files/'):
        os.makedirs('database/files/')
#    connection.close()


# Check dir for files
noise_list = [f for f in os.listdir('noise') if os.path.isfile(''.join(['noise/', f]))]


for file in noise_list:
    seed = obspy.read(''.join(['noise/', file]))
    print('Adding file ', file)

    for trace in seed:
        # Save informations about station
        station_name = trace.stats.station
        utilities.check_and_create_station(station_name, root)

    for i in range(0, len(seed), 3):
        station_id = seed[i].stats.station
        starttime = seed[i].stats.starttime
        # Check if traces are from the same station and save them as event
        if utilities.check_if_same_event(seed, i):
            # Add event to events list
            root.stations[station_id].add_noise(starttime, {seed[i].stats.channel[-1]: seed[i],
                       seed[i + 1].stats.channel[-1]: seed[i+1],
                       seed[i + 2].stats.channel[-1]: seed[i+2]})
        else:
            raise ValueError('There is a problem with event in file ',
                             seed, ' Event starts with trace no ', i)

    # Move used file to another directory
    os.rename(''.join(['noise/',file]), ''.join(['noise/used/', file]))

    transaction.commit()
connection.close()

# Calculate HVSRs for all noise objects at every station
connection.open()
for stat in ['MOSK','PEKW','PCHB']:
    print('Calculating hvsrs for station ', stat)
    i = 0
    for id,noi in root.stations[stat].noise.iteritems():
        if not hasattr(noi, 'calculated_hvsr'):
            print('calculating hvsr for noise no ', str(i))
            noi.calc_hvsr(len_window = 60)
            transaction.commit()
            noi.calculated_hvsr = True
        print('Plotting hvsr for noise no ', str(i))
        root.stations[stat].hvsr_noise[len(root.stations[stat].hvsr_noise)-1].plot_av_hvsr(y_lim=(0,6), show_fig = False)
        i += 1
transaction.commit()
connection.close()