import datetime
import os
import re

import matplotlib.pyplot as plt
import numpy
import persistent
import scipy
import scipy.io
import transaction
from matplotlib.patches import Rectangle

from utilities import konno


class Noise(persistent.Persistent):
    def __init__(self, data, parent):
        self.orig_e = data['E']
        self.orig_n = data['N']
        self.orig_z = data['Z']
        self.e = None
        self.n = None
        self.z = None
        self.cut_channels_to_length()
        self.stats = data['E'].stats

        self.station = parent
        return

    def cut_channels_to_length(self):
        start_times = [0, 0, 0]
        start_times[0] = self.orig_e.stats.starttime
        start_times[1] = self.orig_n.stats.starttime
        start_times[2] = self.orig_z.stats.starttime

        # Look for trace that ends first
        end_times = [0, 0, 0]
        end_times[0] = self.orig_e.stats.endtime
        end_times[1] = self.orig_n.stats.endtime
        end_times[2] = self.orig_z.stats.endtime

        # Slice traces by the same time
        self.e = self.orig_e.slice(starttime=max(start_times),
                                   endtime=min(end_times))
        self.n = self.orig_n.slice(starttime=max(start_times),
                                   endtime=min(end_times))
        self.z = self.orig_z.slice(starttime=max(start_times),
                                   endtime=min(end_times))
        transaction.commit()
        return

    def remove_response(self, trace):
        # Create name
        trace_resp = ''.join([trace, 'resp'])

        # Copy data to another trace
        setattr(self, trace_resp, getattr(self, trace).copy())

        # Detrend data
        getattr(self, trace_resp).detrend()

        # Remove response
        getattr(self, trace_resp).simulate(seedresp={'filename': self.station.dataless, 'units': 'VEL'})

        # Filter resposnse
        getattr(self, trace_resp).filter('bandpass', freqmin=0.5, freqmax=40.0, corners=5)

        return

    def __eq__(self, other):
        return (self.orig_e == other.orig_ehe and
                self.orig_n == other.orig_ehn and
                self.orig_z == other.orig_ehz and
                self.stats == other.stats)

    def calc_hvsr(self, show_diag_message=True, **kwargs):
        self.station.hvsr_noise.insert(len(self.station.hvsr_noise), HvsrNoise(self, **kwargs))
        if show_diag_message:
            print('Calculated!')
        return


class HvsrNoise:
    def __init__(self, parent, **kwargs):

        #
        # Available kwargs:
        #  snr_max - maximum Signal/Noise Ratio (Default - 2),
        #  snr_min - minimum Signal/Noise Ratio (Default - 0.5),
        #  saturation_ratio (Default - 0.995)
        #  sta_time - length of STA window in seconds (Default - 1),
        #  lta_time - length of LTA window in seconds (Default - 20),
        #  overlap - how much big windows can overlap itself (Default - 0.2 of total length)
        #  len_window - length of big window in seconds (Default - 60)
        #  merge_type
        #      geom - geometric mean (default)
        #      arit - arithmetic mean
        #      quad - quadratic mean
        #      harm - harmonic mean
        #  smoothing_type
        #      konno - Konno Ohmachi Smoothing (Default)
        #
        #  freq_width_konno - width of Konno Ohmachi Smoothing in Hz (Default 2Hz), provide int
        #  tapering_perc - how long tapering should be for whole signal (Default - 10%), provide int

        self.signal = parent
        self.station = self.signal.station

        # Create directory for files
        self.directory = ''.join(['database/files/',
                                  self.signal.station.id,
                                  '/hvsr_noise/',
                                  re.sub('[^A-Za-z0-9]+', '', str(self.signal.stats.starttime)),
                                  '_',
                                  re.sub('[^A-Za-z0-9]+', '', str(datetime.datetime.now().time())),
                                  '/'])
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        self.beg_sta = []
        self.end_sta = []
        self.beg_lta = []
        self.end_lta = []
        self.sta = []
        self.lta = []
        self.stalta = []
        self.max_amplitude = []
        self.validity = []
        self.beg_b_window = []
        self.end_b_window = []
        self.hvsr = []
        self.freq_vec = []
        self.no_avg_windows = None
        self.av_freq_vec = None
        self.av_hvsr = None
        self.std_hvsr = None

        self.snr_max = kwargs.get('snr_max', 2)
        self.snr_min = kwargs.get('snr_min', 0.5)
        self.saturation_ratio = kwargs.get('saturation_ratio', 0.995)
        self.sta_time = kwargs.get('len_sta', 1)
        self.lta_time = kwargs.get('len_lta', 20)
        self.overlap = kwargs.get('overlap', 0.2)
        self.len_window = kwargs.get('len_window', 60)
        self.smoothing_type = kwargs.get('smoothing_type', 'konno')
        self.freq_width_konno = kwargs.get('freq_width_konno', 2)
        self.merge_type = kwargs.get('merge_type', 'geom')
        self.tapering_perc = kwargs.get('tapering_perc', 10)

        # Threshold of saturation
        self.saturation_threshold = self.saturation_ratio * max(self.signal.z)

        # Preallocate list for small windows
        self.small_windows = []

        # Calculate length of signal in time
        self.signal_len_time = self.signal.stats.endtime - self.signal.stats.starttime
        self.start_time = self.signal.stats.starttime
        self.end_time = self.signal.stats.endtime

        # Calculate small windows
        self.select_small_windows()

        self.create_big_windows()
        for beg_b_wind, end_b_wind in zip(self.beg_b_window, self.end_b_window):
            self.calc_hvsr(beg_b_wind, end_b_wind)
        self.calc_av_hvsr()

        # Save and delete data of all hvsr from object
        self.save_hvsr_all()
        self.hvsr = None
        self.freq_vec = None

        # Save data of av hvsr
        self.save_hvsr_av()

        return

    def select_small_windows(self):
        # Number of potential windows
        no_potential_windows = int((self.signal_len_time - self.sta_time - self.lta_time) / self.sta_time)

        # Preallocate list for small windows
        self.small_windows = []
        for i in range(no_potential_windows):
            # Calc all possible LTA and STA widnows
            self.beg_sta.append((i - 1) * self.sta_time + self.lta_time)
            self.end_sta.append(i * self.sta_time + self.lta_time)
            self.beg_lta.append(i * self.sta_time)
            self.end_lta.append(i * self.sta_time + self.lta_time)
            # Calc values of STA and LTA for each
            self.sta.append(numpy.mean(numpy.square(
                self.signal.z.slice(self.start_time + self.beg_sta[i], self.start_time + self.end_sta[i]))))

            self.lta.append(numpy.mean(numpy.square(
                self.signal.z.slice(self.start_time + self.beg_lta[i], self.start_time + self.end_lta[i]))))

            self.stalta.append(self.sta[i] / self.lta[i])

            self.max_amplitude.append(
                max(self.signal.z.slice(self.start_time + self.beg_lta[i], self.start_time + self.end_lta[i])))

            self.check_validity(i, self.snr_max, self.snr_min)
        transaction.commit()
        return

    def check_validity(self, i, stalta_max, stalta_min):
        if stalta_min < self.stalta[i] < stalta_max and self.saturation_threshold > self.max_amplitude[i]:
            self.validity.append(True)
        else:
            self.validity.append(False)
        return

    def create_big_windows(self):
        i = 0
        valid_cumsum = 0
        no_valid = int(self.len_window / self.sta_time)

        while i < len(self.validity):
            valid_cumsum += self.validity[i]
            if valid_cumsum == no_valid:
                self.beg_b_window.append(self.beg_sta[i - no_valid + 1])
                self.end_b_window.append(self.end_sta[i])
                valid_cumsum = 0
                i -= int(no_valid * self.overlap)
            else:
                i += 1
        transaction.commit()
        return

    def calc_fft(self, sig, beg_b_win, end_b_win):
        # Get tapering value

        # Cut the signal to provided length
        cutout = sig.slice(self.start_time + beg_b_win,
                           self.start_time + end_b_win).taper(self.tapering_perc)

        # Calc Fourier Transform for the cutout
        cutout_fft = numpy.absolute(numpy.fft.fft(cutout))

        # Calculate frequency vector in Hz
        freq_vec = numpy.fft.fftfreq(cutout.stats.npts, cutout.stats.delta)

        return {'fft': cutout_fft, 'freq_vec': freq_vec,
                'npts': cutout.stats.npts, 'delta': cutout.stats.delta}

    def smooth_fft(self, data):
        # Get frequency vector
        try:
            freq_vec = data['freq_vec']
        except KeyError:
            print('There was no frequency vector provided!')

        # Get fft
        try:
            fft = data['fft']
        except KeyError:
            print('There was no frequency vector provided!')

        # Get delta value
        try:
            delta = data['delta']
        except KeyError:
            print('There was no delta value provided!')

        # Get npts value
        try:
            npts = data['npts']
        except KeyError:
            print('There was no npts value provided!')

        if self.smoothing_type == 'konno':
            if self.freq_width_konno > 0:
                # Calculate the number of points taken to Konno Omachi
                bandwidth = int(self.freq_width_konno * delta * npts)

                smoothed_fft, smoothed_freq_vec = konno(freq_vec,
                                                        bandwidth,
                                                        fft)
        else:
            smoothed_fft = fft
            smoothed_freq_vec = freq_vec

        return smoothed_fft, smoothed_freq_vec

    def merge_channels(self, chan_a, chan_b, ):
        if self.merge_type == 'geom':
            merged = [(x * y) ** 0.5 for x, y in zip(chan_a, chan_b)]
        elif self.merge_type == 'arit':
            merged = [(x + y) * 0.5 for x, y in zip(chan_a, chan_b)]
        elif self.merge_type == 'quad':
            merged = [(x ** 2 + y ** 2) * 0.5 for x, y in zip(chan_a, chan_b)]
        elif self.merge_type == 'harm':
            merged = [2 / ((1 / x) + (1 / y)) for x, y in zip(chan_a, chan_b)]
        else:
            raise ValueError("Type of mean was not properly chosen.")

        return merged

    @staticmethod
    def trim_fft(data):
        # Trim frequency vector to contain only positive frequencies
        data['freq_vec'] = numpy.array([x for x in data['freq_vec'] if x > 0])

        data['fft'] = data['fft'][:len(data['freq_vec'])]

        return data

    def calc_hvsr(self, beg_b_win, end_b_win):

        # Calc fft for each channel trimmed to big window
        e_data = self.calc_fft(self.signal.e, beg_b_win, end_b_win)
        n_data = self.calc_fft(self.signal.n, beg_b_win, end_b_win)
        z_data = self.calc_fft(self.signal.z, beg_b_win, end_b_win)

        # Trim frequency vetors and ffts to contain only positive frequencies
        e_data = self.trim_fft(e_data)
        n_data = self.trim_fft(n_data)
        z_data = self.trim_fft(z_data)

        # Check if all frequency vetors are the same length
        if not (len(z_data['freq_vec']) == len(n_data['freq_vec']) == len(e_data['freq_vec'])):
            raise ValueError('Frequency vectors are not the same length')

        # Smooth the fft of each channel
        e_smooth, e_smooth_freq = self.smooth_fft(e_data)
        n_smooth, n_smooth_freq = self.smooth_fft(n_data)
        z_smooth, z_smooth_freq = self.smooth_fft(z_data)

        en_smooth = self.merge_channels(e_smooth, n_smooth)

        self.hvsr.append([(x / y) for x, y in zip(en_smooth, z_smooth)])
        self.freq_vec.append(e_smooth_freq)

        return

    def calc_av_hvsr(self, no_windows=0):
        # Take as argument number of windows that have to be averaged
        # If a number is not provided, then calculate for all big windows
        if no_windows == 0:
            no_windows = len(self.beg_b_window)

        self.no_avg_windows = no_windows
        # Initialize lists
        hvsr_cat = numpy.array([])
        freq_cat = numpy.array([])

        # Concatenate lists of HVSRS and freqVectors
        for i in range(no_windows):
            hvsr_cat = numpy.concatenate([hvsr_cat, self.hvsr[i]])
            freq_cat = numpy.concatenate([freq_cat, self.freq_vec[i]])

        # Create a numpy array
        data = numpy.transpose(numpy.array([freq_cat, hvsr_cat]))

        # Sort data by rows
        data = data[numpy.argsort(data[:, 0])]
        data = numpy.vstack((data[:, 0], data[:, 1], numpy.log10(data[:, 1])))

        # Preallocate lists
        avg_hv = []
        std_hv = []
        freq_short = []

        no_averged_freqs = int(len(data[0]) / no_windows)

        # Average in every freq range
        for t, l, k in zip(range(no_averged_freqs),
                           range(0, no_averged_freqs * no_windows, no_windows),
                           range(no_windows, no_averged_freqs * no_windows, no_windows)):
            avg_hv.append((sum(data[2, l:k - 1]) - max(data[2, l:k - 1]) - min(data[2, l:k - 1])) / (k - l - 2))

            std_hv.append(((sum((data[2, l:k - 1] - avg_hv[t]) ** 2) - (max(data[2, l:k - 1]) - avg_hv[t]) ** 2 - (
                min(data[2, l:k - 1]) - avg_hv[t]) ** 2) / (k - l - 3)) ** 0.5)

            freq_short.append(sum(data[0, l: k - 1]) / len(data[0, l: k - 1]))

        # Save averaged hvsr
        self.av_freq_vec = freq_short
        self.av_hvsr = [10 ** av for av in avg_hv]
        self.std_hvsr = [(10 ** av) * (10 ** std) * (10 ** std) for av, std in zip(avg_hv, std_hv)]

        # save to database
        transaction.commit()
        return

    def calc_reliability_threshold(self):
        first = 200 / (len(self.beg_b_window) * self.len_window)
        second = 10 / self.len_window
        return max([first, second])

    def plot_av_hvsr(self, show_fig=True, show_table=True, x_lim=(0.2, 40), y_lim=(0, 12)):
        if not show_fig:
            plt.ioff()

        # Load all hvsrs from file
        freq_vec, hvsr = self.load_hvsr_all()
        # Create a figure
        hvlines = plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')

        # Add subplot to create handle to its axes
        ax = hvlines.add_subplot(111)

        # Add description to plot
        ax.set_xlabel('Frequency (Hz)')
        ax.set_ylabel('HVSR')
        ax.set_title(" ".join(
            ['H/V ratio for', self.signal.stats.station, 'station', 'from', str(self.signal.e.stats.starttime)]))
        ax.set_xscale('log')
        ax.set_xlim(x_lim)
        ax.set_ylim(y_lim)

        ax.set_autoscale_on(False)

        # Plot all HVSRs
        ax.plot( hvsr,freq_vec, color='#abc1d4')
        # Plot average HVSR line
        ax.plot(self.av_freq_vec, self.av_hvsr, 'r')
        # Plot Standard deviation HVSR line
        ax.plot(self.av_freq_vec, self.std_hvsr, 'r--')

        current_axis = plt.gca()
        if show_table:
            # Table with HVSR Parameters
            col_labels = ['Window length',
                          'Windows',
                          'Significant cycles',
                          'Reliability threshold']

            table_vals = [[str(self.len_window),
                           str(self.no_avg_windows),
                           str(self.len_window * len(self.beg_b_window)),
                           str(round(self.calc_reliability_threshold(), 2))]]

            plt.table(cellText=table_vals,
                      colWidths=[0.25] * 4,
                      colLabels=col_labels,
                      loc='top',
                      bbox=[0.25, 0.85, 0.7, 0.1])
            # hvlines.subplots_adjust(bottom=0.2)  #Zwiekszenie marginesu

        # Draw unreliable rectangle
        current_axis.add_patch(Rectangle((0, 0), self.calc_reliability_threshold(),
                                         current_axis.get_ylim()[1], facecolor='grey'))
        if show_fig:
            hvlines.show()

        if x_lim == (0, 40):
            #Save to common plots directory
            hvlines.savefig("".join(['plots/', self.signal.stats.station, '_', str(self.start_time.date), '_',
                                     str(self.len_window), '_', str(self.no_avg_windows),
                                     '_noiseHVSR.png']), dpi=150)
            #Save to object's directory
            hvlines.savefig("".join([self.directory, self.signal.stats.station, '_', str(self.start_time.date), '_',
                                     str(self.len_window), '_', str(self.no_avg_windows),
                                     '_noiseHVSR.png']), dpi=150)
        else:
            #Save to common plots directory
            hvlines.savefig("".join(
                ['plots/', self.signal.stats.station, '_x_lim_', str(self.start_time.date), '_', str(self.len_window),
                 '_', str(self.no_avg_windows),
                 '_noiseHVSR.png']), dpi=150)
            # Save to object's directory
            hvlines.savefig("".join(
                [self.directory, self.signal.stats.station, '_x_lim_', str(self.start_time.date), '_', str(self.len_window),
                 '_', str(self.no_avg_windows),
                 '_noiseHVSR.png']), dpi=150)

        if not show_fig:
            hvlines.clf()
            plt.close()

        return

    def export_to_mat(self, directory='hv/', cut=False, lower_limit=1, upper_limit=30):
        name = ''.join(
            [directory, self.signal.station.id, '_noise_'
                                                '_', str(self.signal.stats.starttime.date),
             '_', str(self.len_window), '.mat', ])
        if cut:
            lower_index = list(numpy.floor(self.av_freq_vec)).index(lower_limit)
            upper_index = list(numpy.floor(self.av_freq_vec)).index(upper_limit)
            scipy.io.savemat(name,
                             {'fHV': self.av_freq_vec[lower_index:upper_index],
                              'HVSR': self.av_hvsr[lower_index:upper_index]})
        else:
            scipy.io.savemat(name,
                             {'fHV': self.av_freq_vec,
                              'HVSR': self.av_hvsr})
        return

    # Check if a noise hvsr is based on the same signal
    def check_same_signal(self, other):
        return self.signal == other.signal

    # Find HVSR that are based on the same signals
    def find_hvsrs_from_same_noise(self):
        ret = []
        for no in range(len(self.station.hvsr_noise)):
            if self.check_same_signal(self.station.hvsr_noise[no]):
                ret.append(no)
        return ret

    def save_hvsr_all(self):
        ret = {}
        i = 0

        for hv, freq in zip(self.hvsr, self.freq_vec):
            ret.update({"_".join(['okno', str(i)]): {'fHV': freq, 'HVSR': hv}})
            i += 1

        scipy.io.savemat(''.join([self.directory, 'hvsr_all.mat']), ret)

        return

    def save_hvsr_av(self):

        scipy.io.savemat(''.join([self.directory, 'hvsr_av.mat']),
                         {'freq_vec': self.av_freq_vec,
                          'hvsr': self.av_hvsr,
                          'std_hvsr': self.std_hvsr})

        return

    def load_hvsr_all(self):

        hvsrs = scipy.io.loadmat(''.join([self.directory, 'hvsr_all.mat']))

        hv = []
        freq = []

        for name in hvsrs.keys():
            if '__' in name:
                continue
            freq.append(hvsrs[name].item()[0])
            hv.append(hvsrs[name].item()[1])

        hvsr = [hv[i][0] for i in range(len(hv))]
        freq_vec = [freq[i][0] for i in range(len(freq))]
        return freq_vec, hvsr

    def load_hvsr_av(self):

        return scipy.io.loadmat(''.join([self.directory, 'hvsr_all.mat']))
