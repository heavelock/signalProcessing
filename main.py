import station
import obspy
import os
from obspy.io.xseed import Parser
import transaction
import utilities
import BTrees.OOBTree
import ZODB
import ZODB.FileStorage
import scipy


if 'storage' not in locals():
    storage = ZODB.FileStorage.FileStorage('database\db.fs')
    db = ZODB.DB(storage, pool_size=16, large_record_size = 186405978)
    connection = db.open()
    root = connection.root
    if not os.path.exists('database/files/'):
        os.makedirs('database/files/')
#    connection.close()



# Import list of dataless files
dataless_list = os.listdir(path='dataless')

# Parse xml with list of events and qarthquakes
earthquakes = utilities.parse_event_names('metadata_signals_LGCD.xml')

# Parse matfile with catalog
catalog = utilities.parse_catalog('Catalog_LGCD.mat')

# Check the filelist
file_list = [f for f in os.listdir('dane') if os.path.isfile(''.join(['dane/', f]))]

connection.open()
# Iterate over each file in given list
for seed_name in file_list:
    # Diagnostic printout
    print('Processing ', seed_name, ' file.')
    # Read seed
    seed_data = obspy.read(''.join(['dane/', seed_name]))
    # Parse seed for characteristics

    # Iterate over traces and add events to stations
    for i in range(0, len(seed_data), 3):

        # Check if traces are from the same station and save them as event
        if utilities.check_if_same_event(seed_data, i):
            # Save informations about station
            station_name = seed_data[i].stats.station

            # Check if station exists and create it if needed
            utilities.check_and_create_station(station_name, root)

            # Add event to events list
            root.stations[station_name].add_event(seed_name, earthquakes[seed_name],
                      {seed_data[i].stats.channel[-1]: seed_data[i],
                       seed_data[i + 1].stats.channel[-1]: seed_data[i+1],
                       seed_data[i + 2].stats.channel[-1]: seed_data[i+2]})
        else:
            raise ValueError('There is a problem with event in file ',
                             seed_name, ' Event starts with trace no ', i)

    # root.stations[station_name].events._p_changed = True
    os.rename(''.join(['dane/', seed_name]), ''.join(['dane/used/', seed_name]))
    transaction.commit()
connection.close()

# Check how many events in each station
connection.open()
utilities.check_events()
connection.close()


# Calculate HVSRs for each event and average hvsr
connection.open()
for name in list(root.stations.keys()):
    print('Processing station ', name)
    for event in root.stations[name].events:
        if not hasattr(event, 'hvsr'):
            print('Calculating hvsr for event no ', i)
            event.calc_hvsr()
    if not hasattr(root.stations[name], 'av_hvsr'):
        print('Calculating average HVSR for station', name)
        root.stations[name].calc_av_hvsr()
    root.stations[name].plot_av_hvsr()
connection.close()

# Parse matlab catalog and add info to each event
connection.open()
catalog = utilities.parse_catalog('Catalog_LGCD.mat')
for name in list(root.stations.keys()):
    print('Processing station ', name)
    for event in root.stations[name].events:
        event.add_catalog_data(catalog)
connection.close()

connection.open()


# Add some noise

# Check dir for files
noise_list = [f for f in os.listdir('noise') if os.path.isfile(''.join(['noise/', f]))]


for file in noise_list:
    seed = obspy.read(''.join(['noise/', file]))
    print('Adding file ', file)

    for trace in seed:
        # Save informations about station
        station_name = trace.stats.station
        utilities.check_and_create_station(station_name, root)

    for i in range(0, len(seed), 3):
        station_id = seed[i].stats.station
        starttime = seed[i].stats.starttime
        # Check if traces are from the same station and save them as event
        if utilities.check_if_same_event(seed, i):
            # Add event to events list
            root.stations[station_id].add_noise(starttime, {seed[i].stats.channel[-1]: seed[i],
                       seed[i + 1].stats.channel[-1]: seed[i+1],
                       seed[i + 2].stats.channel[-1]: seed[i+2]})
        else:
            raise ValueError('There is a problem with event in file ',
                             seed, ' Event starts with trace no ', i)

    # Move used file to another directory
    os.rename(''.join(['noise/',file]), ''.join(['noise/used/', file]))

    transaction.commit()
connection.close()

# Calculate HVSRs for all noise objects at every station
connection.open()
for stat in root.stations:
    for noi in root.stations[stat].noise.iteritems():
        noi[1].calc_hvsr()
        transaction.commit()
    root.stations[stat].plot_all_hvsr()
connection.close()

print('Elo elo trzy dwa zero')
