##################################
#
#
#
# Class for station object
#
#
# Author: Damian Kula
# March 2016
##################################
import persistent
import transaction
import numpy
import matplotlib.pyplot as plt
import event
import noise
import BTrees.OOBTree
import os


def create_station_in_db(root, station_parsed):
    # Provide as argument:
    #   - root directory of database
    #   - an dictionary {'station_id':,'station_name':}
    # This type of dict is provided by parsed.get_inventory()['stations']
    # But first, check if a station of that name exists
    if not station_parsed['station_id'][3:] in list(root.stations.keys()):
        root.stations[station_parsed['station_id'][3:]] = \
            Station(station_parsed['station_id'][3:],
                    station_parsed['station_name'])

        transaction.commit()
    return


class Station(persistent.Persistent):

    def __init__(self, station_id):
        self.id = station_id
        self.events = BTrees.OOBTree.BTree()
        self.noise = BTrees.OOBTree.BTree()
        self.hvsr_noise = BTrees.OOBTree.BTree()

        self.dataless = None
        self.dataless_data = None

        if not os.path.exists(''.join(['database/files/',station_id])):
            os.makedirs(''.join(['database/files/',station_id]))
        if not os.path.exists(''.join(['database/files/', station_id,'/hvsr_noise'])):
            os.makedirs(''.join(['database/files/', station_id,'/hvsr_noise']))

        return

    def add_event(self, signal_id, eq_id, data,):
        self.events.insert(signal_id, event.Event(signal_id, eq_id, data, self))
        return

    def add_noise(self,starttime, data):
        self.noise.insert(starttime, noise.Noise(data, self))
        return

    def add_dataless(self, dataless):
        import datetime
        self.dataless = dataless
        self.dataless_date = datetime.datetime.now()
        return


    def how_many_events(self):
        return len(self.events)

    def check_for_duplicated_events(self):
        x = self.events
        x_copy = x[:]
        removed = 0
        for i, a in enumerate(x):
            if any(a == b for b in x_copy[:i - removed]):
                del x_copy[i - removed]
                removed += 1
        return len(x) - len(x_copy)

    def delete_duplicated_events(self):
        x = self.events
        x_copy = x[:]
        removed = 0
        for i, a in enumerate(x):
            if any(a == b for b in x_copy[:i - removed]):
                del x_copy[i - removed]
                removed += 1
            self.events = x_copy[:]

        # Save to database
        transaction.commit()
        return len(x) - len(x_copy)

    def check_for_duplicated_noises(self):
        x = self.noise
        x_copy = x[:]
        removed = 0
        for i, a in enumerate(x):
            if any(a == b for b in x_copy[:i - removed]):
                del x_copy[i - removed]
                removed += 1
        return len(x) - len(x_copy)

    def delete_duplicated_noises(self):
        x = self.noise
        x_copy = x[:]
        removed = 0
        for i, a in enumerate(x):
            if any(a == b for b in x_copy[:i - removed]):
                del x_copy[i - removed]
                removed += 1
            self.events = x_copy[:]

        # Save to database
        transaction.commit()
        return len(x) - len(x_copy)

    def calc_av_hvsr(self):
        # How many events in station?
        n = self.how_many_events()

        # Initialize lists
        hvsr_cat = numpy.array([])
        freq_cat = numpy.array([])

        # Concatenate lists of HVSRS and freqVectors
        for i in range(n):
            hvsr_cat = numpy.concatenate([hvsr_cat, self.events[i].hvsr])
            freq_cat = numpy.concatenate([freq_cat, self.events[i].freq_vec])

        # Create a numpy array
        data = numpy.transpose(numpy.array([freq_cat, hvsr_cat]))

        # Sort data by rows
        data = data[numpy.argsort(data[:, 0])]
        data = numpy.vstack((data[:, 0], data[:, 1], numpy.log10(data[:, 1])))

        # Preallocate lists
        l = 0
        avg_hv = []
        std_hv = []
        freq_short = []

        # Average in every freq range
        for t in range(int(numpy.fix(len(data[0]) / (n)))):
            k = l + n

            avg_hv.append((sum(data[2, l:k - 1]) - max(data[2, l:k - 1]) - min(data[2, l:k - 1])) / (k - l - 2))

            std_hv.append(((sum((data[2, l:k - 1] - avg_hv[t]) ** 2) - (max(data[2, l:k - 1]) - avg_hv[t]) ** 2 - (
                min(data[2, l:k - 1]) - avg_hv[t]) ** 2) / (k - l - 3)) ** 0.5)

            freq_short.append((data[0, l] + data[0, k - 1]) / 2)

            l = k

        # Save averaged hvsr
        self.av_freq_vec = freq_short
        self.av_hvsr = [10 ** av for av in avg_hv]
        self.std_hvsr = [(10 ** av) * (10 ** std) * (10 ** std) for av, std in zip(avg_hv, std_hv)]

        # Commit transaction
        transaction.commit()
        return

    def plot_av_hvsr(self):
        # How many events in station?
        n = self.how_many_events()

        # Create a figure
        HVlines = plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')
        # Plot all HVSRs
        for i in range(n):
            plt.plot(self.events[i].freq_vec,
                     self.events[i].hvsr, color='#abc1d4')


        # Plot average HVSR line
        plt.plot(self.av_freq_vec, self.av_hvsr, 'r')
        # Plot Standard deviation HVSR line
        plt.plot(self.av_freq_vec, self.std_hvsr, 'r--')
        # Add description to plot
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('HVSR')
        plt.title(" ".join(['H/V ratio for',self.id,'station']))
        plt.xscale('log')
        plt.xlim((0.5,30))
        plt.ylim((0,12))
        plt.show()
        HVlines.savefig("".join(['plots/',self.id,'_signalHVSR.png']), dpi=150)


        return



    def plot_all_hvsr(self):
        hv = plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')
        for i in range(len(self.hvsr_noise)):
            if i == 0:
                plt.plot(self.hvsr_noise[i].av_freq_vec, self.hvsr_noise[i].av_hvsr, color='#abc1d4', label = 'Noise HVSR')
            else:
                plt.plot(self.hvsr_noise[i].av_freq_vec, self.hvsr_noise[i].av_hvsr, color='#abc1d4')

        # Plot average HVSR line
        ev_hv = plt.plot(self.av_freq_vec, self.av_hvsr, 'r', label = 'Averaged event HVSR')
        # Add description to plot
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('HVSR')
        plt.title(" ".join(['H/V ratio for',self.id,'station, calculated using noise and events']))
        plt.xscale('log')
        plt.xlim((0.5,30))
        plt.ylim((0,4))
        plt.legend()
        plt.show()
        hv.savefig("".join(['plots/',self.id,'_all_HVSR.png']), dpi=150)
        return

    def save_all_av_hvsr_noise(self):
        data = numpy.array([])
        fre = numpy.array([])
        hv = numpy.array([])
        for i, noi in self.hvsr_noise.iteritems():
            hv = numpy.concatenate((hv, noi.av_hvsr))
            fre = numpy.concatenate((fre, noi.av_freq_vec))
            data = numpy.concatenate((data, [noi.start_time.isoformat()[:-7]] * len(noi.av_freq_vec)))

        dane = numpy.transpose(numpy.array([data, fre, hv]))

        numpy.savetxt(''.join(['database/files/',self.id,'/av_hvsr.txt']), dane, delimiter=';', fmt='%s')
        return
