##################################
#
#
#
# Create a
#
#
# Author: Damian Kula
# March 2016
##################################
import BTrees.OOBTree
from station import create_station_in_db
import utilities
import obspy
from obspy.io.xseed import Parser
import os

def create_structure():
    # Connect to database defined in utilities
    if 'root' not in locals():
        connection, root = utilities.db_connect()

    # Check the filelist
    file_list = os.listdir(path='dane')

    for seed_name in file_list:
        seed_data = obspy.read(''.join(['dane/', seed_name]))
        # Parse seed for characteristics
        parsed = Parser(''.join(['dane/', seed_name]))

        # Create main tree in database
        root.stations = BTrees.OOBTree.BTree()

        for station_parsed in parsed.get_inventory()['stations']:
            create_station_in_db(root, station_parsed)
    return

