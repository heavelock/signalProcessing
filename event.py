##################################
#
#
#
# Class for event object
#
#
# Author: Damian Kula
# March 2016
##################################

import persistent
import numpy
import matplotlib.pyplot as plt
import transaction
import obspy.signal.filter
from utilities import konno
import scipy.io
import datetime
import re
import os


def calc_log_e(mw):
    return 1.15 + 1.96 * mw


class Event(persistent.Persistent):
    def __init__(self, signal_id, eq_id, data, parent):
        # Init channel holders
        self.orig_e = data['E']
        self.orig_n = data['N']
        self.orig_z = data['Z']

        #Assign parent station
        self.station = parent

        # Save stats
        self.stats = data['E'].stats

        # Determine signal type in way compatible with response removal function
        self.signal_type = self.determine_signal_type()

        # Save signal and earthquake id
        self.signal_id = signal_id
        self.earthquake_id = eq_id

        # # Remove response
        # self.remove_response('e')
        # self.remove_response('n')
        # self.remove_response('z')

        # Allocate hvsr and freq_vec lists
        self.hvsr = None
        self.freq_vec = None

        # Allocate acceleration parameters
        self.a_e = None
        self.a_n = None
        self.a_z = None
        self.a_en = None
        self.a_enz = None
        self.a10_e = None
        self.a10_n = None
        self.a10_z = None
        self.a10_en = None
        self.a10_enz = None

        # Allocate velocity parameters
        self.v_max_e = None
        self.v_max_n = None
        self.v_max_z = None
        self.v_max_en = None
        self.v_max_enz = None


        # Allocate attributes for catalog
        self.cat_time = None
        self.cat_lat = None
        self.cat_lon = None
        self.cat_depth = None
        self.cat_elevation = None
        self.cat_mw = None
        self.logE = None


        return

    def determine_signal_type(self):
        if self.stats.channel[0] == 'C':
            ret = 'ACC'
        else:
            ret = 'VEL'
        return ret

    def save_and_delete_orig_signals(self):
        ret = {'orig_e': self.orig_e,
               'orig_n': self.orig_n,
               'orig_z': self.orig_z}

        scipy.io.savemat(''.join([self.directory, 'original_signals.mat']), ret)

        # Clear saved variables
        self.orig_e = None
        self.orig_n = None
        self.orig_z = None
        return

    def save_and_delete_signals(self):
        ret = {'orig_e': self.e,
               'orig_n': self.n,
               'orig_z': self.z}

        scipy.io.savemat(''.join([self.directory, 'signals.mat']), ret)

        # Clear saved variables
        self.e = None
        self.n = None
        self.z = None
        return

    def load_signals(self):
        loaded = scipy.io.loadmat(''.join([self.directory, 'signals.mat']))
        return (loaded['e'],loaded['n'],loaded['z'])

    def cut_channels_to_length(self):

        start_times = [0, 0, 0]
        start_times[0] = self.orig_e.stats.starttime
        start_times[1] = self.orig_n.stats.starttime
        start_times[2] = self.orig_z.stats.starttime

        # Look for trace that ends first
        end_times = [0, 0, 0]
        end_times[0] = self.orig_e.stats.endtime
        end_times[1] = self.orig_n.stats.endtime
        end_times[2] = self.orig_z.stats.endtime

        # Slice and return traces by the same time
        return (self.orig_e.slice(starttime=max(start_times),
                                  endtime=min(end_times)),
                self.orig_n.slice(starttime=max(start_times),
                                  endtime=min(end_times)),
                self.orig_z.slice(starttime=max(start_times),
                                  endtime=min(end_times)))

    def remove_response(self, trace):

        # Copy data to another trace
        trace_resp = getattr(self, trace).copy()

        # Detrend data
        trace_resp.detrend()

        # Filter signal with response
        trace_resp.filter('bandpass', freqmin=0.5, freqmax=40.0, corners=5)

        # Remove response
        trace_resp.simulate(seedresp={'filename': self.station.dataless, 'units': self.signal_type})
        #
        #
        #
        # NIEBEZPIECZNE JAK CHOLERA, BO ZAKLADA, ZE JEST KAZDY SYGNAL ACC!
        #
        #
        #
        #

        # Filter signal without response
        trace_resp.filter('bandpass', freqmin=0.5, freqmax=40.0, corners=5)

        return trace_resp

    def add_earthquake_id(self, earthquake_id):
        self.earthquake_id = earthquake_id
        return

    def calc_hvsr(self, freq_width_konno=2):
        e,n,z = self.load_signals()
        # Calculate FFTfor each channel
        post_fft_e = numpy.absolute(numpy.fft.fft(e.data))
        post_fft_n = numpy.absolute(numpy.fft.fft(n.data))
        post_fft_z = numpy.absolute(numpy.fft.fft(z.data))

        sampling = 1 / self.stats.sampling_rate
        sig_len = self.stats.npts

        # Create a frequency vector
        freq_vector = numpy.arange(round(1 / (sig_len * sampling), 2), 1 / sampling,
                                   round(1 / (sig_len * sampling), 2))
        # Cut out frequencies higher than 30
        freq_vector = [x for x in freq_vector if x <= 40]
        freq_vector = numpy.array(freq_vector)
        # Cut the postFFT data to freqVec
        post_fft_e = post_fft_e[:len(freq_vector)]
        post_fft_n = post_fft_n[:len(freq_vector)]
        post_fft_z = post_fft_z[:len(freq_vector)]

        # Check if have to apply Konno Omachi
        if freq_width_konno > 0:
            # Calculate the number of points taken to Konno Omachi
            band_for_konno = int(numpy.fix(freq_width_konno * sampling * sig_len))

            post_konno_ehe, post_konno_freq_vector = konno(freq_vector,
                                                           band_for_konno,
                                                           post_fft_e)
            post_konno_ehn, post_konno_freq_vector = konno(freq_vector,
                                                           band_for_konno,
                                                           post_fft_n)
            post_konno_ehz, post_konno_freq_vector = konno(freq_vector,
                                                           band_for_konno,
                                                           post_fft_z)

        else:
            # Skip smoothing, just assign variable
            post_konno_ehe = post_fft_e
            post_konno_ehn = post_fft_n
            post_konno_ehz = post_fft_z
            post_konno_freq_vector = freq_vector

        # Merge E with N using geometric mean
        post_konno_ehen = [(x * y) ** 0.5 for x, y in zip(post_konno_ehe, post_konno_ehn)]

        # calculate and save HVSR and freq vector
        self.hvsr = [(x / y) for x, y in zip(post_konno_ehen, post_konno_ehz)]
        self.freq_vec = post_konno_freq_vector

        transaction.commit()
        return

    # Plot the HVSR curve
    def plot_hvsr(self):
        # Create a figure
        hv_plot = plt.figure(num=None, figsize=(15, 10), dpi=150, facecolor='w', edgecolor='k')

        ax1 = hv_plot.add_subplot(111)
        # Plot all HVSRs
        ax1.plot(self.freq_vec, self.hvsr)

        # Add description to plot
        ax1.set_xlabel('Frequency (Hz)')
        ax1.set_ylabel('HVSR')
        ax1.set_title(" ".join(['H/V ratio for', self.stats.station, 'station']))
        ax1.set_xscale('log')
        ax1.set_xlim((0.5, 30))
        ax1.setylim((0, 12))
        return

    # Plot all channels from signal
    def plot_signals(self, response_removed=True):
        signals = plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')

        ax1 = signals.add_subplot(311)
        ax2 = signals.add_subplot(312)
        ax3 = signals.add_subplot(313)

        if response_removed:
            ax1.plot(self.e_resp.times(), self.e_resp.data, 'k')
            ax2.plot(self.n_resp.times(), self.n_resp.data, 'k')
            ax3.plot(self.z_resp.times(), self.z_resp.data, 'k')
        else:
            ax1.plot(self.e.times(), self.e.data, 'k')
            ax2.plot(self.n.times(), self.n.data, 'k')
            ax3.plot(self.z.times(), self.z.data, 'k')

        # Titleplots
        ax1.set_title('EHE')
        ax2.set_title('EHN')
        ax3.set_title('EHZ')
        return

    # comparison of to events
    def __eq__(self, other):
        ret = self.orig_e == other.orig_EHE and self.orig_n == other.orig_EHN and \
              self.orig_z == other.orig_EHZ and self.stats == other.stats and \
              self.signal_id == other.signal_id
        return ret

    # add data from parsed catalog to event object
    def add_catalog_data(self, catalog):
        no = catalog['eq_id'].index(self.earthquake_id)
        self.cat_time = catalog['time'][no]
        self.cat_lat = catalog['lat'][no]
        self.cat_lon = catalog['lon'][no]
        self.cat_depth = catalog['depth'][no]
        self.cat_elevation = catalog['elevation'][no]
        self.cat_mw = catalog['mw'][no]
        self.logE = calc_log_e(self.cat_mw)
        transaction.commit()
        return

    # Print asic data from object
    def print_data(self):
        dict_keys = ['earthquake_id', 'signal_id', 'cat_time',
                     'cat_lon', 'cat_lat', 'cat_depth',
                     'cat_elevation', 'cat_mw', 'logE']
        for key in dict_keys:
            print('{0}:  {1}'.format(key, getattr(self, key)))
        return

    def calc_a_max(self,trace_e, trace_n, trace_z):
        # Calculate signals for unfiltered signals
        self.a_e = max(abs(trace_e.data))
        self.a_n = max(abs(trace_n.data))
        self.a_z = max(abs(trace_z.data))
        self.a_en = max([(e ** 2 + n ** 2) ** 0.5 for e, n in zip(trace_e.data,
                                                                  trace_n.data)])
        self.a_enz = max([(e ** 2 + n ** 2 + z ** 2) ** 0.5 for e, n, z in zip(trace_e.data,
                                                                               trace_n.data,
                                                                               trace_z.data)])

        # Filter signals
        e_fil_lowpass = trace_e.copy()
        e_fil_lowpass.filter('lowpass', freq = self.lowpass_filter_freq, corners = self.lowpass_filter_order)

        n_fil_lowpass = trace_n.copy()
        n_fil_lowpass.filter('lowpass', freq = self.lowpass_filter_freq, corners = self.lowpass_filter_order)

        z_fil_lowpass = trace_z.copy()
        z_fil_lowpass.filter('lowpass', freq = self.lowpass_filter_freq, corners = self.lowpass_filter_order)


        # Calc parameters for filtered signals
        self.a10_e = max(abs(e_fil_lowpass.data))
        self.a10_n = max(abs(n_fil_lowpass.data))
        self.a10_z = max(abs(z_fil_lowpass.data))
        self.a10_en = max([(e ** 2 + n ** 2) ** 0.5 for e, n in zip(e_fil_lowpass,
                                                                    n_fil_lowpass)])
        self.a10_enz = max([(e ** 2 + n ** 2 + z ** 2) ** 0.5 for e, n, z in zip(e_fil_lowpass,
                                                                                 n_fil_lowpass,
                                                                                 z_fil_lowpass)])

        return

    @staticmethod
    def draw_plots_orig_and_filtered(e, n, z, filter_type, filter_freq=10, filter_order=5):
        # AKA Ogladactwo

        # Create a figure
        hvlines = plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')

        # Add subplot to create handle to its axes
        ax1 = hvlines.add_subplot(321)
        ax2 = hvlines.add_subplot(322)
        ax3 = hvlines.add_subplot(323)
        ax4 = hvlines.add_subplot(324)
        ax5 = hvlines.add_subplot(325)
        ax6 = hvlines.add_subplot(326)
        # Add labels to plots
        # ax1.set_xlabel('Acceleration (m/s^2)')
        ax1.set_ylabel('Acceleration (m/s^2)')
        # ax2.set_xlabel('Acceleration (m/s^2)')
        # ax2.set_ylabel('Time (s)')
        # ax3.set_xlabel('Acceleration (m/s^2)')
        ax3.set_ylabel('Acceleration (m/s^2)')
        # ax4.set_xlabel('Acceleration (m/s^2)')
        # ax4.set_ylabel('Time (s)')
        ax5.set_xlabel('Time (s)')
        ax5.set_ylabel('Acceleration (m/s^2)')
        ax6.set_xlabel('Time (s)')
        # ax6.set_ylabel('Time (s)')

        ax1.set_title('Original Signals')
        ax2.set_title('Filtered Signals')

        # ax1.set_title(" ".join(
        #     ['Earthquake no ', self.earthquake_id, 'recorded on station ', self.station.id]))
        # Plot all channels
        ax1.plot(e.times(), e, color='#abc1d4')
        ax3.plot(n.times(), n, color='#abc1d4')
        ax5.plot(z.times(), z, color='#abc1d4')

        # Filter signals
        e_fil_filtered = e.copy()
        e_fil_filtered.filter(filter_type, freq=filter_freq, corners=filter_order)

        n_fil_filtered = n.copy()
        n_fil_filtered.filter(filter_type, freq=filter_freq, corners=filter_order)

        z_fil_filtered = z.copy()
        z_fil_filtered.filter(filter_type, freq=filter_freq, corners=filter_order)

        # Plot all channels filtered by lowpass
        ax2.plot(e_fil_filtered.times(), e_fil_filtered.data, color='#abc1d4')
        ax4.plot(n_fil_filtered.times(), n_fil_filtered.data, color='#abc1d4')
        ax6.plot(z_fil_filtered.times(), z_fil_filtered.data, color='#abc1d4')

        # Add additional information about available actions
        plt.annotate('', (0, 0), (0, -20), xycoords='axes fraction', textcoords='offset points', va='top')

        hvlines.show()

        return

    # Function makes copy of given trace and integrates it with cumtrapz.
    # New trace's name is composition of name of given trace with '_vel'
    def acc_to_vel(self, trace, filter_type = 'highpass', filter_freq = 1, filter_order = 5):
        trace_vel =  trace.copy()

        # Filter data
        trace_vel.filter(filter_type, freq=filter_freq, corners=filter_order)

        # Integrate data
        trace_vel.integrate(method='cumtrapz')

        return trace_vel

    def draw_vel_plots(self, trace_e, trace_n, trace_z, filter_freq=1, filter_order=5):
        # Create a figure
        hvlines = plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')

        # Add subplot to create handle to its axes
        ax1 = hvlines.add_subplot(321)
        ax2 = hvlines.add_subplot(322)
        ax3 = hvlines.add_subplot(323)
        ax4 = hvlines.add_subplot(324)
        ax5 = hvlines.add_subplot(325)
        ax6 = hvlines.add_subplot(326)
        # Add labels to plots
        # ax1.set_xlabel('Acceleration (m/s^2)')
        ax1.set_ylabel('Velocity (m/s)')
        # ax2.set_xlabel('Acceleration (m/s^2)')
        # ax2.set_ylabel('Time (s)')
        # ax3.set_xlabel('Acceleration (m/s^2)')
        ax3.set_ylabel('Velocity (m/s)')
        # ax4.set_xlabel('Acceleration (m/s^2)')
        # ax4.set_ylabel('Time (s)')
        ax5.set_xlabel('Time (s)')
        ax5.set_ylabel('Velocity (m/s)')
        ax6.set_xlabel('Time (s)')
        # ax6.set_ylabel('Time (s)')

        ax1.set_title('Original Acceletarion Signals')
        ax2.set_title('Processed Velocity Signals')

        # ax1.set_title(" ".join(
        #     ['Earthquake no ', self.earthquake_id, 'recorded on station ', self.station.id]))
        # Plot all channels
        ax1.plot(trace_e.times(), trace_e.data, color='#abc1d4')
        ax3.plot(trace_n.times(), trace_n.data, color='#abc1d4')
        ax5.plot(trace_z.times(), trace_z.data, color='#abc1d4')

        # Filter signals
        e_vel = self.acc_to_vel(trace_e, filter_freq=filter_freq, filter_order=filter_order)
        n_vel = self.acc_to_vel(trace_n, filter_freq=filter_freq, filter_order=filter_order)
        z_vel = self.acc_to_vel(trace_z, filter_freq=filter_freq, filter_order=filter_order)

        # Plot all channels filtered by lowpass
        ax2.plot(e_vel.times(), e_vel.data, color='#abc1d4')
        ax4.plot(n_vel.times(), n_vel.data, color='#abc1d4')
        ax6.plot(z_vel.times(), z_vel.data, color='#abc1d4')

        # Add additional information about available actions
        plt.annotate('', (0, 0), (0, -20), xycoords='axes fraction', textcoords='offset points', va='top')

        hvlines.show()

        return e_vel, n_vel, z_vel

    # Applies given filter to 3 given traces in object
    def apply_filter_to_traces(self, e, n, z, filter_type='highpass', filter_freq=1, filter_order=5):
        getattr(self, e).filter(filter_type, freq=filter_freq, corners=filter_order)
        getattr(self, n).filter(filter_type, freq=filter_freq, corners=filter_order)
        getattr(self, z).filter(filter_type, freq=filter_freq, corners=filter_order)
        return

    # Calculate velocity parameters based on velocity traces
    def calc_v_max(self, trace_e, trace_n, trace_z):
        self.v_max_e = max(abs(trace_e.data))
        self.v_max_n = max(abs(trace_n.data))
        self.v_max_z = max(abs(trace_z.data))

        self.v_max_en = max([numpy.sqrt(x**2+y**2) for x, y in zip(trace_e.data,
                                                                   trace_n.data)])
        self.v_max_enz = max([numpy.sqrt(x ** 2 + y ** 2 + z ** 2) for x, y, z in zip(trace_e.data,
                                                                                      trace_n.data,
                                                                                      trace_z.data)])
        return

    def lasting_time(self):

        return
